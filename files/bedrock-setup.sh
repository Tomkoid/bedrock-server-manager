mkdir bedrock
cd bedrock

echo Getting MCPE 1.19.63.01..
wget https://minecraft.azureedge.net/bin-linux/bedrock-server-1.19.63.01.zip -q
unzip bedrock-server-1.19.63.01.zip
chmod +x bedrock_server

cd ..

echo cd bedrock > run-server.sh
echo ./bedrock_server >> run-server.sh
chmod +x run-server.sh

clear

echo Modifying alias..
echo alias bedrock="cd && cd Bedrock && ./run-server.sh" > b.sh
source b.sh
rm b.sh

clear

echo Done!
