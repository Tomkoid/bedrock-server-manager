# CD into $HOME directory
cd

# Download uninstall script
echo Getting uninstall script..
wget https://raw.githubusercontent.com/TheDarknessToma/bedrock-server-manager/main/files/uninstall.sh -q

# Create directories
mkdir Bedrock
mkdir PocketMine
mkdir Nukkit

# Get all scripts
echo Getting all bedrock scripts..
wget https://gitlab.com/Tomkoid/bedrock-server-manager/-/raw/main/files/bedrock-setup.sh -O Bedrock/bedrock-setup.sh -q
wget https://gitlab.com/Tomkoid/bedrock-server-manager/-/raw/main/files/bedrock-setup-custom-zip.sh -O Bedrock/bedrock-setup-custom-zip.sh -q

echo Getting all pocketmine scripts..
wget https://gitlab.com/Tomkoid/bedrock-server-manager/-/raw/main/files/pocketmine-setup.sh -O PocketMine/pocketmine-setup.sh -q
wget https://gitlab.com/Tomkoid/bedrock-server-manager/-/raw/main/files/pocketmine-reinstall.sh -O PocketMine/pocketmine-reinstall.sh -q
wget https://gitlab.com/Tomkoid/bedrock-server-manager/-/raw/main/files/pocketmine-plugins.sh -O PocketMine/pocketmine-plugins -q

echo Getting nukkit-setup.sh..
wget https://gitlab.com/Tomkoid/bedrock-server-manager/-/raw/main/files/nukkit-setup.sh -O Nukkit/nukkit-setup.sh -q
echo Getting installall script..
wget https://gitlab.com/Tomkoid/bedrock-server-manager/-/raw/main/files/installall.sh -O installall.sh -q

# Change permissions for scripts
echo Marking uninstall script as executable..
chmod +x uninstall.sh

echo Marking all bedrock scripts as executable..
chmod +x Bedrock/bedrock-setup.sh
chmod +x Bedrock/bedrock-setup-custom-zip.sh

echo Marking all pocketmine scripts as executable..
chmod +x PocketMine/pocketmine-setup.sh
chmod +x PocketMine/pocketmine-reinstall.sh
chmod +x PocketMine/pocketmine-plugins.sh

echo Marking nukkit-setup.sh as executable..
chmod +x Nukkit/nukkit-setup.sh

echo Marking installall script as executable..
chmod +x installall.sh

# Create aliases
alias bedrock='cd && cd Bedrock && source bedrock-setup.sh'
alias pocketmine='cd && cd PocketMine && source pocketmine-setup.sh'
alias nukkit='cd && cd Nukkit && source nukkit-setup.sh'
alias uninstall_bedrock='cd && source uninstall.sh'
clear

echo Done!
echo
echo You can run Bedrock server with "bedrock" command.
echo You can run PocketMine server with "pocketmine" command.
echo You can run Nukkit server with "nukkit" command.
echo
echo Uninstall with "uninstall_bedrock" command.
